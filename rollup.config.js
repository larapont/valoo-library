import path from 'path'

import { uglify } from 'rollup-plugin-uglify'
import babel from 'rollup-plugin-babel'
import replace from 'rollup-plugin-replace'
import commonjs from 'rollup-plugin-commonjs'
import resolve from 'rollup-plugin-node-resolve'
import postcss from 'rollup-plugin-postcss'

// import postcssModules from 'postcss-modules'
// const cssExportMap = {}

const onwarn = warning => {
  // Silence circular dependency warning for moment package
  if (
    warning.code === 'CIRCULAR_DEPENDENCY'
    && !warning.importer.indexOf(path.normalize('node_modules/moment/src/lib/'))
  ) {
    return
  }

  console.warn(`(!) ${warning.message}`)
}

const config = {
  input: 'src/components/index.js',
  onwarn,
  external: ['react', 'react-dom'],
  plugins: [
    postcss({
      extensions: ['.scss']
    }),
    babel({
      exclude: 'node_modules/**',
      extensions: ['.js', '.jsx', '.ts', '.tsx', '.css']
    }),
    resolve({
      mainFields: ['main', 'jsnext']
    }),
    commonjs(),
    uglify(),
    replace({
      // ENV: JSON.stringify(process.env.NODE_ENV || 'development')
      'process.env.NODE_ENV': JSON.stringify('development')
    })
  ],
  output: {
    format: 'umd',
    name: 'valLibrary',
    globals: {
      react: 'React',
      'react-dom': 'ReactDOM'
    }
  }
}
export default config
