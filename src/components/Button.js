import React, { Component } from 'react'

import './Button.scss'

class Button extends Component {
  render () {
    let { children } = this.props
    return (
      <button className='button'>{children}</button>
    )
  }
}
export default Button
