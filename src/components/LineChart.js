import React, { PureComponent } from 'react'
import Chart from "chart.js";

//--Chart Style Options--//
Chart.defaults.global.responsive = true;
Chart.defaults.global.maintainAspectRation = false;
Chart.defaults.global.elements.line.fill = false;
//--Chart Style Options--//

let myLineChart;

export default class LineChart extends PureComponent {
  chartRef = React.createRef();

  componentDidMount() {
    this.buildChart();
  }
  
  componentDidUpdate() {
    this.buildChart();
  }

  buildChart = () => {
    const myChartRef = this.chartRef.current.getContext("2d")
    const { data, options, labels } = this.props
        
    if (typeof myLineChart !== 'undefined') {
      myLineChart.destroy();      
    }
    
    myLineChart = new Chart(myChartRef, {
      type: "line",
      data: {
        labels: labels,
        datasets: data
      },
      options: options
    });
  }


  render() {
    return (
      <div className='line-chart'>
        <canvas
          id="myChart"
          ref={this.chartRef}
        />
      </div>
    )
  }
}
