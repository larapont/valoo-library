import React from 'react'

import { Button, LineChart } from './components'

function App () {
  const labels = ['Jan', 'Feb', 'March']
  const datasets = [{
    label: 'Sales',
    data: [86, 50, 91],
    borderColor: '#ff4785'
  },
  {
    label: 'Purchases',
    data: [100, 20, 71],
    // fill: false,
    borderColor: '#1EA7FD'
  }]
  return (
    <div className='App'>
      <LineChart labels={labels} data={datasets} />
      <Button>Hola</Button>
    </div>
  )
}

export default App
