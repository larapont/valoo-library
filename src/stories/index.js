import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'

import { Button, Welcome } from '@storybook/react/demo'
import { LineChart } from '../components'

storiesOf('Welcome', module).add('to Storybook', () => <Welcome showApp={linkTo('Button')} />);

storiesOf('Button', module)
  .add('with text', () => <Button onClick={action('clicked')}>Hello Button</Button>)
  .add('with some emoji', () => (
    <Button onClick={action('clicked')}>
      <span role="img" aria-label="so cool">
        😀 😎 👍 💯
      </span>
    </Button>
  ))

const labels = ['Jan', 'Feb', 'March']
const datasets = [{
  label: 'Sales',
  data: [86, 50, 91],
  borderColor: '#ff4785'
},
{
  label: 'Purchases',
  data: [100, 20, 71],
  // fill: false,
  borderColor: '#1EA7FD'
}]

storiesOf('LineChart', module)
  .add('default', () => <LineChart labels={labels} data={datasets} />)